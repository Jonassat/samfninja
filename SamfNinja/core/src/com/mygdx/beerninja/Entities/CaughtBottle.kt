package com.mygdx.beerninja.Entities

class CaughtBottle(var id: Int, var time: Float, var xcoor: Float, var ycoor: Float, var playerID: String, var points: Int)