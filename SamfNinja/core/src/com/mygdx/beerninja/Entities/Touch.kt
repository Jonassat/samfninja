package com.mygdx.beerninja.Entities

import com.badlogic.gdx.graphics.Texture
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/*
Touch ENTITY, represents sprite to be rendered on screen
Part of the ECS pattern

 */

@JsonIgnoreProperties("texture")
class Touch(var id: Int, enemy: Boolean, private var textures: HashMap<String, Texture>) {
    var x: Int = 0
    var y: Int = 0
    var display = false
    var texture: Texture? = null

    private fun getTexture(enemy: Boolean): Texture? {
        return if (!enemy) {
            textures["myTouch"]
        } else textures["enemyTouch"]
    }

    init {
        texture = getTexture(enemy)
    }
}
